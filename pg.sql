--
-- classroom (postgres db)
--

SET client_encoding = 'UTF8';
SET client_min_messages = warning;

-- students

CREATE TABLE IF NOT EXISTS students (
    uniqid serial PRIMARY KEY,
    year integer NOT NULL,
    class character varying(4) NOT NULL,
    division character varying(4) NOT NULL,
    rollno integer NOT NULL,
    name text,
    loginct integer NOT NULL,
	UNIQUE (year, class, division, rollno)
);

-- lectures

CREATE TABLE IF NOT EXISTS lectures (
    uniqid serial PRIMARY KEY,
    class character varying(4) NOT NULL,
    division character varying(4) NOT NULL,
    createtime timestamp without time zone NOT NULL,
    active boolean NOT NULL,
    uniqname text UNIQUE NOT NULL,
    hints text
);

-- questions

CREATE TABLE IF NOT EXISTS questions (
    uniqid serial PRIMARY KEY,
    description text NOT NULL
);

-- attendance

CREATE TABLE IF NOT EXISTS attendance (
    lecid integer NOT NULL REFERENCES lectures(uniqid) ON UPDATE CASCADE ON DELETE CASCADE,
    studentid integer NOT NULL REFERENCES students(uniqid) ON UPDATE CASCADE ON DELETE CASCADE,
    lastlogin timestamp without time zone NOT NULL,
    hostaddrs text[] NOT NULL,
	PRIMARY KEY (lecid, studentid)
);

-- lecqsets

CREATE TABLE IF NOT EXISTS lecqsets (
    lecid integer NOT NULL REFERENCES lectures(uniqid) ON UPDATE CASCADE ON DELETE CASCADE,
    questid integer NOT NULL REFERENCES questions(uniqid) ON UPDATE CASCADE ON DELETE CASCADE,
    orderkey integer NOT NULL,
	PRIMARY KEY (lecid, questid)
);

-- responses

CREATE TABLE IF NOT EXISTS responses (
    uniqid serial PRIMARY KEY,
    studentid integer NOT NULL REFERENCES students(uniqid) ON UPDATE CASCADE ON DELETE CASCADE,
    lecid integer NOT NULL,
    questid integer NOT NULL,
    response text NOT NULL,
    submittime timestamp without time zone NOT NULL,
	FOREIGN KEY (lecid, questid) REFERENCES lecqsets(lecid, questid) ON UPDATE CASCADE ON DELETE CASCADE
);

-- --

-- DROP TABLE responses;
-- DROP TABLE lecqsets;
-- DROP TABLE attendance;
-- DROP TABLE questions;
-- DROP TABLE lectures;
-- DROP TABLE students;