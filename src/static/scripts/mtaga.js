function mtagaUpdated(pre) {
  var text = pre.innerText;
  if (text == "" || text == "\n") {
    pre.dataset.phshow = ""; // show placeholder
    return "";
  } else {
    delete pre.dataset.phshow; // hide placeholder
    return text;
  }
}

function makeGreatAgain(ta) {
  ta.style.display = "none";
  var pre = document.createElement("pre");
  pre.oninput = function() {
    ta.value = mtagaUpdated(pre);
    ta.dispatchEvent(new Event('input'));
  };
  pre.innerText = ta.value || "\n";
  pre.dataset.ph = ta.placeholder;
  pre.contenteditable = true;
  pre.setAttribute("contenteditable", "true");
  pre.classList.add("made-great");
  ta.parentNode.insertBefore(pre, ta);
  mtagaUpdated(pre);
}

window.addEventListener("load", function() {
  var tas = document.querySelectorAll("textarea[data-make-great-again]");
  for(let i = 0; i < tas.length; i++) {
    makeGreatAgain(tas[i]);
  }
});
