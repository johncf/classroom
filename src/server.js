"use strict";

const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const cookieSession = require('cookie-session')

const app = express();
const util = require('./util')

app.set('view engine', 'pug');
app.set('views', __dirname + '/views');
//app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cookieSession({
  keys: [ util.config.signsecret ],
  maxAge: 2*60*60*1000,
}));

app.use(express.static(__dirname + '/static'));

app.get('/', (req, res) => {
  res.render('index');
})

const teacher = require('./teacher')
app.use('/teacher', teacher)

const student = require('./student')
app.use('/student', student)

app.listen(util.config.port, () => {
  console.log('Listening on port ' + util.config.port)
});
