# Programming Classroom

## Setup (Linux)

1.  Install Node.js and PostgreSQL.
2.  Create a database named `classroom` and run `pg.sql` query.
3.  Update Node.js packages by running `npm install`.
4.  Create a `config.json` file in the project root directory by copying
    [the example](./config.json.example) and making appropriate changes.
5.  To start the server, run `npm start` from the project root.

## Setup (Windows)

1.  Install Node.js and PostgreSQL.
2.  Build an installer with [NSIS][] using [`installer.nsi` script][].
3.  The installer\* tries to configure the app appropriately using valid paths
    to dependencies, and generates `config.json` and useful batch scripts.
4.  To start the server, use the new shortcut on Desktop named `CS Lab`, or
    execute the batch script `run.bat` that the shortcut points to.

\* It is more of a glorified extractor than a full-fledged 'installer', for it
runs in user-mode and makes no changes to the system except for extracting the
contents and creating a Desktop shortcut. To 'uninstall', delete the directory
to which it was extracted, delete the Dektop shortcut, and delete the Postgres
database that was newly created.

[NSIS]: http://nsis.sourceforge.net/
[`installer.nsi` script]: ./installer.nsi
