"use strict";

const util = require('./util')

const express = require('express')
const router = express.Router()
const relDate = require('relative-date')
const md = require('markdown-it')()

function require_auth(req, res, next) {
  if (req.session.teacher == "1") {
    next();
  } else {
    res.redirect(303, '/teacher')
  }
}

router.get(['/', '/login'], (req, res) => {
  if (req.session.teacher == "1") {
    res.redirect(303, '/teacher/lectures')
  } else {
    res.render('teacher/login')
  }
});

router.post('/login', (req, res) => {
  if (req.body.password == util.config.teachpass) {
    req.session.teacher = "1";
    res.redirect(303, '/teacher/lectures');
  } else {
    res.render('teacher/login', { error: "Wrong password" });
  }
});

router.get('/logout', (req, res) => {
  req.session.teacher = null
  res.redirect(303, '/teacher')
});

router.get('/lectures', require_auth, (req, res) => {
  const page = +req.query.page || 0;
  util.pg.query("SELECT uniqid, uniqname, class, division, createtime, active, " +
                       "(SELECT COUNT(DISTINCT a.studentid) " +
                        "FROM attendance a " +
                        "WHERE lecid = l.uniqid) AS acount " +
                "FROM lectures l ORDER BY createtime DESC " +
                "LIMIT 20 OFFSET $1", [page * 20])
    .then((qres) => {
      let lecs = qres.rows.map((row) => ({
        id: row.uniqid,
        key: row.uniqname,
        class: row.class,
        div: row.division,
        reldate: relDate(row.createtime),
        active: row.active,
        acount: row.acount
      }));
      res.render('teacher/lecture-list', { year: util.config.acadyear, lectures: lecs, page: page });
    })
    .catch((err) => {
      console.error('Error while fetching lectures:', err);
      res.render('teacher/lecture-list', { error: err.message });
    });
});

router.post('/lecture-create', require_auth, (req, res) => {
  let lec = {
    key: req.body.key.trim() || null,
    cls: req.body.cls.trim() || null,
    div: req.body.div.trim() || null,
  };
  util.pg
    .query("INSERT INTO lectures(class, division, createtime, active, uniqname) " +
           "VALUES ($1, $2, NOW(), true, $3)" +
           "RETURNING uniqid", [lec.cls, lec.div, lec.key])
    .then((qres) => {
      res.redirect(303, '/teacher/lecture/' + qres.rows[0].uniqid);
    })
    .catch((err) => {
      console.error('Error while creating lecture:', err);
      res.status(500).render('error', { message: err.message });
    });
});

router.get('/lecture/:lecid', require_auth, (req, res) => {
  var lec = { id: req.params.lecid }
  var attn = null;
  util.pg
    .query("SELECT class, division, createtime, active, uniqname, hints FROM lectures WHERE uniqid = $1", [lec.id])
    .then((qres) => {
      if (qres.rowCount == 0) {
        throw new Error("Lecture #" + lec.id + " not found!")
      }
      let row = qres.rows[0];
      lec.cname = row.class + ' ' + row.division;
      lec.createtime = relDate(row.createtime);
      lec.isactive = row.active;
      lec.name = row.uniqname;
      lec.hints = row.hints;
      return util.pg.query(
          "WITH r AS (SELECT r.studentid, array_agg(ARRAY[r.uniqid, lq.orderkey, extract(epoch from r.submittime)]) as rlist " +
                     "FROM responses r INNER JOIN lecqsets lq ON r.lecid = lq.lecid AND " +
                                                                "r.questid = lq.questid " +
                     "WHERE r.lecid = $1 " +
                     "GROUP BY r.studentid) " +
          "SELECT s.rollno, a.lastlogin, a.hostaddrs, r.rlist " +
          "FROM attendance a INNER JOIN students s ON s.uniqid = a.studentid " +
               "LEFT OUTER JOIN r ON r.studentid = a.studentid " +
          "WHERE a.lecid = $1 " +
          "ORDER BY s.rollno", [lec.id])
    })
    .then((qres) => {
      attn = qres.rows.map((row) => ({
        roll: row.rollno,
        intime: relDate(row.lastlogin),
        addrs: row.hostaddrs,
        rlist: row.rlist ? row.rlist.sort((a, b) => a[2] == b[2] ? 0 : a[2] < b[2] ? -1 : 1) : [],
      }));
      return util.pg.query("SELECT lq.questid, lq.orderkey, q.description FROM lecqsets lq INNER JOIN questions q ON q.uniqid = lq.questid WHERE lq.lecid = $1 ORDER BY lq.orderkey", [lec.id]);
    })
    .then((qres) => {
      res.render('teacher/lecture', { lec: lec, attn: attn, ques: qres.rows });
    })
    .catch((err) => {
      console.error('Error while fetching lecture details:', err);
      res.status(500).render('error', { message: err.message });
    });
});

router.get('/lecture/:lecid/preview', require_auth, (req, res) => {
  const lecid = req.params.lecid;
  var dict = { profile: { year: util.config.acadyear }, preview: true };
  util.pg.query("SELECT class, division, hints FROM lectures WHERE uniqid = $1", [lecid])
    .then((qres) => {
      if (qres.rowCount == 0) {
        throw new Error("Lecture #" + lecid + " not found!")
      }
      let row = qres.rows[0];
      dict.profile.class = row.class;
      dict.profile.div = row.division;
      dict.hints = row.hints ? md.render(row.hints) : "";
      return util.pg.query("SELECT lqs.orderkey AS qkey, q.uniqid AS qid, q.description AS qtext " +
                           "FROM questions q INNER JOIN lecqsets lqs ON lqs.questid = q.uniqid " +
                           "WHERE lqs.lecid = $1 " +
                           "ORDER BY lqs.orderkey ASC", [lecid])
    })
    .then((qres) => {
      dict.questions = qres.rows.map((row) => ({
        qkey: row.qkey,
        qid: row.qid,
        qtext: md.render(row.qtext),
      }));
      res.render('student/lecture', dict);
    })
    .catch((err) => {
      console.error('While rendering student home:', err);
      res.status(500).render('error', { message: err.message });
    });

});

router.post('/lecture/:lecid/qadd', require_auth, (req, res) => {
  res.set('Content-Type', 'text/plain');
  let lec = {
    qid: req.body.qid.trim() || null,
    key: req.body.qorder.trim() || null,
  };
  util.pg.query("INSERT INTO lecqsets (lecid, questid, orderkey) VALUES ($1, $2, $3) " +
                "ON CONFLICT (lecid, questid) DO UPDATE SET orderkey = excluded.orderkey",
                [req.params.lecid, lec.qid, lec.key])
    .then((qres) => {
      res.status(200).send('success');
    })
    .catch((err) => {
      console.error('Error while adding lecture question:', err);
      res.status(500).send(err.message);
    });
});

router.post('/lecture/:lecid/qremove', require_auth, (req, res) => {
  res.set('Content-Type', 'text/plain');
  let qid = req.body.qid.trim() || null;
  util.pg.query("DELETE FROM lecqsets WHERE lecid = $1 AND questid = $2",
                [req.params.lecid, qid])
    .then((qres) => {
      res.status(200).send('success');
    })
    .catch((err) => {
      console.error('Error while removing lecture question:', err);
      res.status(500).send(err.message);
    });
});

router.post('/lecture/:lecid/hints', require_auth, (req, res) => {
  res.set('Content-Type', 'text/plain');
  let hints = req.body.hints.trim() || null;
  util.pg.query("UPDATE lectures SET hints = $2 WHERE uniqid = $1",
                [req.params.lecid, hints])
    .then((qres) => {
      res.status(200).send('success');
    })
    .catch((err) => {
      console.error('Error while updating hints:', err);
      res.status(500).send(err.message);
    });
});

router.post('/lecture/:lecid/switch', require_auth, (req, res) => {
  res.set('Content-Type', 'text/plain');
  util.pg.query("UPDATE lectures SET active = NOT active WHERE uniqid = $1",
                [req.params.lecid])
    .then((qres) => {
      res.status(200).send('success');
    })
    .catch((err) => {
      console.error('Error while ending lecture:', err);
      res.status(500).send(err.message);
    });
});

router.get('/lecture/:lecid/responses/:qid', require_auth, (req, res) => {
  var lresp = { lecid: req.params.lecid, qid: req.params.qid };
  util.pg.query("SELECT r.uniqid, s.rollno, r.response, r.submittime " +
                "FROM responses r INNER JOIN students s ON r.studentid = s.uniqid " +
                "WHERE r.lecid = $1 AND r.questid = $2 " +
                "ORDER BY s.rollno, r.submittime", [lresp.lecid, lresp.qid])
    .then((qres) => {
      var proll = null;
      lresp.rcount = 0;
      lresp.scount = 0;
      lresp.responses = qres.rows.map((row) => {
        lresp.rcount += 1;
        if (row.rollno != proll) {
          lresp.scount += 1;
          proll = row.rollno;
        }
        return {
          id: row.uniqid,
          rollno: row.rollno,
          response: row.response,
          time: relDate(row.submittime)
        }
      });
      res.render('teacher/lecture-responses', lresp);
    });
});

router.get('/response', require_auth, (req, res) => {
  res.set('Content-Type', 'text/plain');
  util.pg.query("SELECT response FROM responses WHERE uniqid = $1", [req.query.rid])
    .then((qres) => {
      if (qres.rowCount == 1) {
        res.status(200).send(qres.rows[0].response);
      } else {
        res.status(404).send("Response #" + req.query.rid + " not found!");
      }
    })
    .catch((err) => {
      console.error('Error while fetching response:', err);
      res.status(500).send(err.message);
    });
});

router.post('/response/delete', require_auth, (req, res) => {
  res.set('Content-Type', 'text/plain');
  util.pg.query("DELETE FROM responses WHERE uniqid = $1", [req.body.rid])
    .then((qres) => {
      if (qres.rowCount == 1) {
        res.status(200).send("success");
      } else {
        res.status(400).send("Response #" + req.body.rid + " not found!");
      }
    })
    .catch((err) => {
      console.error('Error while deleting response:', err);
      res.status(500).send(err.message);
    });
});

router.post('/questions/new', require_auth, (req, res) => {
  let qtext = req.body.text.trim() || null;
  res.set('Content-Type', 'text/plain');
  util.pg.query("INSERT INTO questions(description) VALUES ($1)" +
                "RETURNING uniqid", [qtext])
    .then((qres) => {
      res.status(200).send("id: " + qres.rows[0].uniqid);
    })
    .catch((err) => {
      console.error('Error while inserting question:', err);
      res.status(500).send(err.message);
    });
});

router.post('/questions/delete', require_auth, (req, res) => {
  let qid = req.body.qid.trim() || null;
  res.set('Content-Type', 'text/plain');
  util.pg.query("DELETE FROM questions WHERE uniqid = $1", [qid])
    .then((qres) => {
      if (qres.rowCount == 1) {
        res.status(200).send("success");
      } else {
        res.status(500).send("No question with uniqid `" + qid + "` found.");
      }
    })
    .catch((err) => {
      console.error('Error while deleting question:', err);
      res.status(500).send(err.message);
    });
});

router.get('/questions', require_auth, (req, res) => {
  const page = +req.query.page || 0;
  util.pg.query("SELECT uniqid, description, (SELECT COUNT(r.uniqid) " +
                                             "FROM responses r " +
                                             "WHERE questid = q.uniqid) AS acount " +
                       "FROM questions q " +
                       "ORDER BY uniqid DESC " +
                       "LIMIT 12 OFFSET $1", [page * 12])
    .then((qres) => {
      let questions = qres.rows.map((row) => ({
        id: row.uniqid,
        src: row.description,
        text: md.render(row.description),
        acount: row.acount
      }));
      res.render('teacher/questions', { questions: questions, page: page });
    })
    .catch((err) => {
      console.error('Error while inserting question:', err);
      res.status(500).send(err.message);
    });
});

module.exports = router
