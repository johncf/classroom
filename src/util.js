"use strict";

// from read-package-json
function stripBOM(content) {
    // Remove byte order marker. This catches EF BB BF (the UTF-8 BOM)
    // because the buffer-to-string conversion in `fs.readFileSync()`
    // translates it to FEFF, the UTF-16 BOM.
    if (content.charCodeAt(0) === 0xFEFF) {
        content = content.slice(1);
    }
    return content;
}

const configStr = require('fs').readFileSync(__dirname + '/../config.json', 'utf8');
const config = JSON.parse(stripBOM(configStr));
const pgpool = new require('pg').Pool(config.db);

module.exports.config = config;
module.exports.pg = pgpool;
