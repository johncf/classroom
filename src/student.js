"use strict";

const util = require('./util')

const express = require('express')
const md = require('markdown-it')()
const router = express.Router()

function require_auth(req, res, next) {
  if (req.session.student) {
    next();
  } else {
    res.redirect(303, '/student')
  }
}

router.get(['/', '/login'], (req, res) => {
  if (req.session.student) {
    res.redirect(303, '/student/lecture')
  } else {
    res.render('student/login')
  }
});

router.post('/login', (req, res) => {
  if (req.session.student) {
    res.redirect(303, '/student/lecture');
    console.log("Re-login attempt from", req.connection.remoteAddress, "by", req.session.student.id);
    return;
  }
  const form = {
    roll: req.body.roll.trim() || null,
    key: req.body.key.trim() || null
  };
  if (!form.roll || !form.key) {
    res.render('student/login', { form: form, error: "Fields cannot be empty!" });
    return;
  }
  var lecid, studentid;
  util.pg.query("SELECT uniqid, class, division, active FROM lectures " +
                "WHERE uniqname = $1", [form.key])
    .then((qres) => {
      if (qres.rows.length == 0) {
        throw new Error("No active lecture with key " + form.key + " found");
      } else if (qres.rows[0].active == false) {
        throw new Error("Lecture '" + form.key + "' has ended");
      } else {
        lecid = qres.rows[0].uniqid;
        let qdata = [
          util.config.acadyear, // FIXME automatically calculate
          qres.rows[0].class,
          qres.rows[0].division,
          form.roll,
        ];
        return util.pg.query("INSERT INTO students(year, class, division, rollno, loginct) " +
                             "VALUES ($1, $2, $3, $4, 0) " +
                             "ON CONFLICT (year, class, division, rollno) " +
                                         "DO UPDATE SET loginct=students.loginct + 1 " +
                             "RETURNING uniqid", qdata);
      }
    })
    .then((qres) => {
      studentid = qres.rows[0].uniqid;
      let qdata = [
        lecid,
        studentid,
        [req.connection.remoteAddress],
      ];
      return util.pg.query("INSERT INTO attendance(lecid, studentid, lastlogin, hostaddrs) " +
                           "VALUES ($1, $2, NOW(), $3) " +
                           "ON CONFLICT (lecid, studentid) " +
                                       "DO UPDATE SET lastlogin=NOW(), " +
                                                     "hostaddrs=attendance.hostaddrs || EXCLUDED.hostaddrs",
                           qdata);
    })
    .then((qres) => {
      req.session.student = { id: studentid, lecid: lecid };
      res.redirect(303, '/student/lecture');
    })
    .catch((err) => {
      console.error('While student log in:', err);
      res.status(500).render('error', { message: err.message });
    });
});

router.get('/lecture', require_auth, (req, res) => {
  const lecid = req.session.student.lecid;
  var dict = { profile: { id: req.session.student.id } };
  util.pg.query("SELECT year, class, division, rollno, loginct FROM students " +
                "WHERE uniqid = $1", [dict.profile.id]) // TODO name
    .then((qres) => {
      if (qres.rowCount == 0) {
        throw new Error("Invalid student found in session!")
      }
      dict.profile.year = qres.rows[0].year,
      dict.profile.class = qres.rows[0].class,
      dict.profile.div = qres.rows[0].division,
      dict.profile.roll = qres.rows[0].rollno,
      dict.profile.count = qres.rows[0].loginct
      return util.pg.query("SELECT lqs.orderkey AS qkey, q.uniqid AS qid, q.description AS qtext " +
                           "FROM questions q INNER JOIN lecqsets lqs ON lqs.questid = q.uniqid " +
                           "WHERE lqs.lecid = $1 " +
                           "ORDER BY lqs.orderkey ASC", [lecid])
    })
    .then((qres) => {
      dict.questions = qres.rows.map((row) => ({
        qkey: row.qkey,
        qid: row.qid,
        qtext: md.render(row.qtext),
        rcount: 0,
      }));
      return util.pg.query("SELECT hints FROM lectures WHERE uniqid = $1", [lecid]);
    })
    .then((qres) => {
      let hints = qres.rows[0].hints;
      dict.hints = hints ? md.render(hints) : "";
      return util.pg.query("SELECT questid, COUNT(*) AS rcount FROM responses WHERE lecid = $1 AND studentid = $2 GROUP BY questid", [lecid, dict.profile.id]);
    })
    .then((qres) => {
      for (let row of qres.rows) {
        for (let q of dict.questions) {
          if (row.questid == q.qid) {
            q.rcount = row.rcount;
          }
        }
      }
      res.render('student/lecture', dict);
    })
    .catch((err) => {
      console.error('While rendering student home:', err);
      res.status(500).render('error', { message: err.message });
    });
});

router.post('/submit', require_auth, (req, res) => {
  const lecid = req.session.student.lecid;
  const studentid = req.session.student.id;
  const questid = req.body.qid;
  const response = req.body.response || null; // trim right?
  res.set('Content-Type', 'text/plain');
  util.pg.query("INSERT INTO responses(lecid, studentid, questid, response, submittime) " +
                "VALUES ($1, $2, $3, $4, NOW()) ", [lecid, studentid, questid, response])
    .then((qres) => {
      res.status(200).send('success');
    })
    .catch((err) => {
      console.error('While submitting a response:', err);
      res.status(500).send(err.message);
    })
});

router.get('/logout', (req, res) => {
  req.session.student = null
  res.redirect(303, '/student')
});

module.exports = router
