function get_resobj(form, resclass) {
  var resobj = form.getElementsByClassName(resclass)[0];
  if (resobj) {
    resobj.innerText = "";
    resobj.classList.remove("form200");
    resobj.classList.remove("formfail");
  }
  return resobj;
}

function submit_simple(resobj, httpstatus, body) {
  if (!resobj) return;
  if (httpstatus == 200) {
    resobj.classList.add("form200");
    resobj.innerText = body;
  } else {
    resobj.classList.add("formfail");
    resobj.innerText = "Err" + httpstatus + ": " + body;
  }
}

function submit_reload(resobj, httpstatus, body) {
  submit_simple(resobj, httpstatus, body);
  if (httpstatus == 200) {
    this.reset();
    window.location.reload();
  }
}

function ajaxsubmit(form, oncomplete, resclass) {
  resclass = resclass || "formresult";
  oncomplete = oncomplete || submit_simple;
  var resobj = get_resobj(form, resclass);
  var kvpairs = [];
  for (var i = 0; i < form.elements.length; i += 1) {
    var elem = form.elements[i];
    if (elem.name) {
      kvpairs.push(elem.name + '=' + window.encodeURIComponent(elem.value));
    }
  }
  const formdata = kvpairs.join('&');
  const xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
      oncomplete.call(form, resobj, xhr.status, xhr.responseText);
    }
  };
  if (form.method == "get") {
    let url = form.action + "?" + formdata;
    xhr.open("GET", url);
    xhr.send();
  } else {
    xhr.open("POST", form.action);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(formdata);
  }
  return xhr;
}
