Name "CS Lab"
OutFile "CS Lab Setup.exe"
InstallDir "$EXEDIR\CSLab"
ShowInstDetails show
RequestExecutionLevel user
;SetCompressor lzma

!include "FileFunc.nsh"
!include "LogicLib.nsh"
!include "MUI2.nsh"
!include "nsDialogs.nsh"
!include "x64.nsh"

; --- pages

; node directory select
Var NodeDir

Var NodeDirBox

Page custom NodePagePre NodePageLeave

; postgres directory select
Var PgUser
Var PgPass
Var PgDb
Var PgDir

Var PgUserBox
Var PgPassBox
Var PgDbBox
Var PgDirBox

Page custom PgPagePre PgPageLeave

; install directory select
!insertmacro MUI_PAGE_DIRECTORY

; installation progress
!insertmacro MUI_PAGE_INSTFILES

; finish page with option to run init.bat
!define MUI_FINISHPAGE_TEXT "Before running the post-install script, make sure you have an active internet connection. If the script exits with errors, simply run this setup again after trying to resolve it. It is perfectly fine to 'install' multiple times to the same destination. Also note, to 'uninstall', just delete it!"
!define MUI_FINISHPAGE_RUN "$INSTDIR\init.bat"
!define MUI_FINISHPAGE_RUN_TEXT "Run post-install script (init.bat)"
!define MUI_FINISHPAGE_SHOWREADME
!define MUI_FINISHPAGE_SHOWREADME_FUNCTION ShowReadme
!define MUI_FINISHPAGE_SHOWREADME_TEXT "View config.json (incl. Postgres password)"
!insertmacro MUI_PAGE_FINISH

; --- pages end

!insertmacro MUI_LANGUAGE "English"

; --- callbacks

Function NodePagePre
  StrCpy $NodeDir "$PROGRAMFILES\nodejs"
  IfFileExists "$NodeDir\node.exe" 0 +2
  Return
  ${If} ${RunningX64}
    StrCpy $NodeDir "$PROGRAMFILES64\nodejs"
  ${EndIf}

  !insertmacro MUI_HEADER_TEXT "Node.JS Directory" "Verify the path to your Node.JS installation."

  nsDialogs::Create 1018
  Pop $0

  ${If} $0 == error
    Abort
  ${EndIf}

  ${NSD_CreateGroupBox} 10% 45u 80% 34u "Node.JS Install Path"
  Pop $0
    ${NSD_CreateDirRequest} 15% 59u 49% 12u "$NodeDir"
    Pop $NodeDirBox
    ${NSD_CreateBrowseButton} 66% 58u 19% 14u "Browse..."
    Pop $0
    ${NSD_OnClick} $0 OnDirBrowse
    nsDialogs::SetUserData $0 $NodeDirBox ; Set associated DirRequest as user data

  ${NSD_CreateLabel} 0% 124u 100% 12u "Note: The specified directory will be validated when you click Next."
  nsDialogs::Show
FunctionEnd

Function NodePageLeave
  IfFileExists "$NodeDir\node.exe" +3 0
  MessageBox MB_OK "Could not find node.exe in the specified directory!"
  Abort
FunctionEnd

Function PgPagePre
  StrCpy $PgDir "$PROGRAMFILES\PostgreSQL\10"
  ${Locate} "$PROGRAMFILES\PostgreSQL" "/L=D /M=bin" PgDirFound
  ${If} ${RunningX64}
    ${Locate} "$PROGRAMFILES64\PostgreSQL" "/L=D /M=bin" PgDirFound
  ${EndIf}

  !insertmacro MUI_HEADER_TEXT "PostgreSQL Settings" "CS Lab will be configured to use the PostgreSQL settings provided below."

  nsDialogs::Create 1018
  Pop $0

  ${If} $0 == error
    Abort
  ${EndIf}

  ${NSD_CreateGroupBox} 10% 0 80% 34u "Create a new PostgreSQL Database"
  Pop $0
    ${NSD_CreateLabel} 15% 16u 20% 10u "Database Name:"
    Pop $0
    ${NSD_CreateText} 40% 14u 45% 12u "cslab"
    Pop $PgDbBox
  ${NSD_CreateGroupBox} 10% 40u 80% 48u "PostgreSQL User Credentials"
  Pop $0
    ${NSD_CreateLabel} 15% 56u 20% 10u "Username:"
    Pop $0
    ${NSD_CreateText} 40% 54u 45% 12u "postgres"
    Pop $PgUserBox
    ${NSD_CreateLabel} 15% 70u 20% 10u "Password:"
    Pop $0
    ${NSD_CreatePassword} 40% 68u 45% 12u ""
    Pop $PgPassBox

  ${NSD_CreateGroupBox} 10% 94u 80% 34u "PostgreSQL Install Path"
  Pop $0
    ${NSD_CreateDirRequest} 15% 108u 49% 12u "$PgDir"
    Pop $PgDirBox
    ${NSD_CreateBrowseButton} 66% 107u 19% 14u "Browse..."
    Pop $0
    ${NSD_OnClick} $0 OnDirBrowse
    nsDialogs::SetUserData $0 $PgDirBox ; Set associated DirRequest as user data

  nsDialogs::Show
FunctionEnd

Function PgPageLeave
  ${NSD_GetText} $PgUserBox $PgUser
  ${NSD_GetText} $PgPassBox $PgPass
  ${NSD_GetText} $PgDbBox $PgDb
  ${NSD_GetText} $PgDirBox $PgDir
  IfFileExists "$PgDir\bin\psql.exe" +3 0
  MessageBox MB_OK "Could not find bin\psql.exe in the specified directory!"
  Abort
FunctionEnd

Function OnDirBrowse
  Exch $0
  nsDialogs::GetUserData $0 ; Get associated DirRequest from user data
  Pop $1
  Exch $0
  ${NSD_GetText} $1 $2
  nsDialogs::SelectFolderDialog "Select Directory" "$2" 
  Pop $0
  ${If} $0 != error
    ${NSD_SetText} $1 "$0"
  ${EndIf}
FunctionEnd

Function PgDirFound
  StrCpy $0 ""
  IfFileExists "$R9\psql.exe" 0 +3
  StrCpy $PgDir "$R8" ; $R8 is the "tail" part of $R9
  StrCpy $0 StopLocate
  Push $0
FunctionEnd

Function ShowReadme
  Exec 'notepad "$INSTDIR\config.json"'
FunctionEnd

; --- callbacks end

Section
  SetOutPath $INSTDIR
  File /r /x "*.nsi" /x ".git*" /x "junk" /x "CS Lab" /x "*.exe" *

  ; write init.bat
  FileOpen $0 '$INSTDIR\init.bat' w
  FileWrite $0 '@echo off$\r$\n'
  FileWrite $0 'cd /d "%~dp0"$\r$\n'
  FileWrite $0 'echo Initializing Postgres databases...$\r$\n'
  FileWrite $0 'set PGUSER=$PgUser$\r$\n'
  FileWrite $0 'set PGPASSWORD=$PgPass$\r$\n'
  FileWrite $0 '"$PgDir\bin\createdb" $PgDb$\r$\n'
  FileWrite $0 '"$PgDir\bin\psql" -f pg.sql $PgDb$\r$\n'
  FileWrite $0 'echo Downloading Node.JS modules...$\r$\n'
  FileWrite $0 'call "$NodeDir\npm" install$\r$\n'
  FileWrite $0 'pause$\r$\n'
  FileClose $0

  ; write run.bat
  FileOpen $0 '$INSTDIR\run.bat' w
  FileWrite $0 '@echo off$\r$\n'
  FileWrite $0 'cd /d "%~dp0"$\r$\n'
  FileWrite $0 'start http://localhost:8080/teacher$\r$\n'
  FileWrite $0 '"$NodeDir\npm" start$\r$\n'
  FileClose $0

  ; write config.json
  FileOpen $0 '$INSTDIR\config.json' w
  FileWrite $0 '{$\r$\n'
  FileWrite $0 '    "db": {$\r$\n'
  FileWrite $0 '        "host": "localhost",$\r$\n'
  FileWrite $0 '        "port": 5432,$\r$\n'
  FileWrite $0 '        "database": "$PgDb",$\r$\n'
  FileWrite $0 '        "user": "$PgUser",$\r$\n'
  FileWrite $0 '        "password": "$PgPass"$\r$\n'
  FileWrite $0 '    },$\r$\n'
  ${GetTime} "" LS $3 $2 $1 $3 $3 $3 $3
  ${If} $2 < 5 ; 2018-03-01 is in the academic year of 2017-18.
    IntOp $1 $1 - 1
  ${EndIf}
  FileWrite $0 '    "acadyear": $1,$\r$\n'
  FileWrite $0 '    "teachpass": "password",$\r$\n'
  System::Call 'advapi32::SystemFunction036(*i 0 r1, i 2)' ; get a 16-bit random number into $1
  FileWrite $0 '    "signsecret": "some-random-string-for-session-validation-$1",$\r$\n'
  FileWrite $0 '    "port": 8080$\r$\n'
  FileWrite $0 '}$\r$\n'
  FileClose $0

  CreateShortcut "$DESKTOP\CS Lab.lnk" "$INSTDIR\run.bat"
SectionEnd
